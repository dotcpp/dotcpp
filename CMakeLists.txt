cmake_minimum_required(VERSION 3.13)

project(dotcpp VERSION 1.2.0)
enable_testing()

option(COPY_SMALL_BINS "Execute a command at the end of the build to copy the resulting exe to small-bins" OFF)

configure_file(src/config.h.in config.h)

include(cmake/htdocs.cmake)

add_htdocs_file("htdocs/css/styles.css" HTDOCS_STYLES)
add_htdocs_file("htdocs/js/scripts.js" HTDOCS_SCRIPTS)
add_htdocs_file("htdocs/help.html" HTDOCS_HELPHTML)
add_htdocs_file("htdocs/index.html" HTDOCS_INDEXHTML)
add_htdocs_file("htdocs/index-section1.html" HTDOCS_INDEXSECTION1HTML)
add_htdocs_file("htdocs/index-section2.html" HTDOCS_INDEXSECTION2HTML)
add_htdocs_file("htdocs/index-section3.html" HTDOCS_INDEXSECTION3HTML)
add_htdocs_file("htdocs/js/jquery-3.6.0.min.js" HTDOCS_JQUERY)

configure_file(src/htdocs.h.in htdocs.h)

add_executable(dotcpp    
    htdocs/css/styles.css
    htdocs/js/scripts.js
    htdocs/help.html
    htdocs/index.html
    htdocs/index-section1.html
    htdocs/index-section2.html
    htdocs/index-section3.html
    src/commands/abstractcommand.h
    src/commands/buildcommand.cpp
    src/commands/buildcommand.h
    src/commands/cleancommand.cpp
    src/commands/cleancommand.h
    src/commands/commands.cpp
    src/commands/newcommand.cpp
    src/commands/newcommand.h
    src/commands/runcommand.cpp
    src/commands/runcommand.h
    src/commands/servecommand.cpp
    src/commands/servecommand.h
    src/commands/testcommand.cpp
    src/commands/testcommand.h
    src/common/codemodel.cpp
    src/common/codemodel.h
    src/common/httpclient.cpp
    src/common/httpclient.h
    src/common/projecttemplate.cpp
    src/common/projecttemplate.h
    src/common/utils.cpp
    src/common/utils.h
    src/program.cpp
    thirdparty/miniz/miniz.c
    thirdparty/miniz/miniz.h
    thirdparty/nlohmann/json.hpp
    thirdparty/cpp-httplib/httplib.h
)

target_include_directories(dotcpp
    PUBLIC
        "thirdparty/"
        "${PROJECT_BINARY_DIR}"
)

target_link_libraries(dotcpp
    PUBLIC wininet
)

target_compile_features(dotcpp
    PRIVATE cxx_std_17
    PRIVATE cxx_auto_type
    PRIVATE cxx_nullptr
    PRIVATE cxx_range_for
)

if(COPY_SMALL_BINS)
    ADD_CUSTOM_COMMAND(
        TARGET dotcpp
        POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:dotcpp> ../small-bins/
    )
endif()

add_subdirectory(tests)
