#ifndef RUNCOMMAND_H
#define RUNCOMMAND_H

#include "abstractcommand.h"

#include <filesystem>
#include <nlohmann/json.hpp>

class RunCommand : public AbstractCommand
{
public:
    RunCommand();

    RunCommand(
        std::unique_ptr<AbstractCommand> nextCommand);

    bool ParseCommandLine(
        const std::vector<std::string> &args);

protected:
    virtual int InternalRun();

    virtual void PrintCommandError();

    virtual void PrintCommandHelp();

private:
    std::string _target;
    std::string _configuration;
    std::filesystem::path _projectPath;
    std::string _targetToRun;

    std::vector<std::filesystem::path> FindRunnableArtifacts(
        const nlohmann::json &target,
        const nlohmann::json &configuration,
        const std::filesystem::path &cmakeFileApiReplyPath);
};

#endif // RUNCOMMAND_H
