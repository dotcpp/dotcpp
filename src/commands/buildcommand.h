#ifndef BUILDCOMMAND_H
#define BUILDCOMMAND_H

#include "abstractcommand.h"

#include <filesystem>

class BuildCommand : public AbstractCommand
{
public:
    BuildCommand();

    BuildCommand(
        std::unique_ptr<AbstractCommand> nextCommand);

    bool ParseCommandLine(
        const std::vector<std::string> &args);

protected:
    virtual int InternalRun();

    virtual void PrintCommandError();

    virtual void PrintCommandHelp();

private:
    std::string _target;
    std::string _configuration;
    std::filesystem::path _projectPath;

    int BuildCmakeProject();
};

#endif // BUILDCOMMAND_H
