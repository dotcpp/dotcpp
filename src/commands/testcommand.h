#ifndef TESTCOMMAND_H
#define TESTCOMMAND_H

#include "abstractcommand.h"

class TestCommand : public AbstractCommand
{
public:
    TestCommand();

    TestCommand(
        std::unique_ptr<AbstractCommand> nextCommand);

    bool ParseCommandLine(
        const std::vector<std::string> &args);

protected:
    virtual int InternalRun();

    virtual void PrintCommandError();

    virtual void PrintCommandHelp();

private:
    std::filesystem::path _projectPath;
};

#endif // TESTCOMMAND_H
