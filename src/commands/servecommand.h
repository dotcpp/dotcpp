#ifndef SERVECOMMAND_H
#define SERVECOMMAND_H

#include "abstractcommand.h"

#include "../common/codemodel.h"
#include "../common/httpclient.h"
#include <atomic>
#include <filesystem>
#include <iostream>
#include <thread>
#include <vector>

class ServeCommand : public AbstractCommand
{
public:
    ServeCommand();

    ServeCommand(
        std::unique_ptr<AbstractCommand> nextCommand);

    bool ParseCommandLine(
        const std::vector<std::string> &args);

    enum class States
    {
        EmptyFolder = 0,
        Generating = 1,
        Idle = 2,
        Building = 3,
        Testing = 4,
        Running = 5,
        Cleaning = 6,
    };

protected:
    virtual int InternalRun();

    virtual void PrintCommandError();

    virtual void PrintCommandHelp();

    template <class TCommand>
    void StartCommand(
        States state,
        const std::vector<std::string> &args)
    {
        auto fn = [&](States s, const std::vector<std::string> &args) {
            _state.store(s);

            TCommand command;
            command.ParseCommandLine(args);
            command.Run();

            _state.store(ServeCommand::States::Idle);

            _codeModel.Load(_projectPath);
        };

        _workers
            .push_back(std::thread(fn, state, args));
    }

private:
    int _port = 8008;
    std::filesystem::path _projectPath;
    CodeModel _codeModel;
    std::atomic<States> _state;
    std::vector<std::thread> _workers;

    int ServeCmakeProject();

    void StartBuildCommand(
        const std::string &configuration);

    void StartCleanCommand(
        const std::string &configuration);

    void StartTestCommand();

    void StartNewProjectCommand(
        const std::string &templateName);

    nlohmann::json GetState();

    nlohmann::json GetConfigurations();

    nlohmann::json GetTargets(
        const std::string &configuration);

    nlohmann::json GetTargetFiles(
        const std::string &configuration,
        const std::string &targetName);

    nlohmann::json GetTargetFile(
        const std::string &configuration,
        const std::string &targetName,
        const std::string &fileName);

    nlohmann::json AddFile(
        const std::string &configuration,
        const std::string &targetName,
        const std::string &fileName,
        const std::string &body);

    nlohmann::json UpdateFile(
        const std::string &configuration,
        const std::string &targetName,
        const std::string &fileName,
        const std::string &body);
};

#endif // SERVECOMMAND_H
