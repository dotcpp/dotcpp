#ifndef CLEANCOMMAND_H
#define CLEANCOMMAND_H

#include "abstractcommand.h"

#include <filesystem>

class CleanCommand : public AbstractCommand
{
public:
    CleanCommand();

    CleanCommand(
        std::unique_ptr<AbstractCommand> nextCommand);

    bool ParseCommandLine(
        const std::vector<std::string> &args);

protected:
    virtual int InternalRun();

    virtual void PrintCommandError();

    virtual void PrintCommandHelp();

private:
    std::filesystem::path _projectPath;

    int CleanCmakeProject();
};

#endif // CLEANCOMMAND_H
