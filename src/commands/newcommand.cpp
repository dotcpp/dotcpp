#include "newcommand.h"

#include "../common/httpclient.h"
#include "../common/utils.h"
#include "buildcommand.h"
#include "runcommand.h"
#include <algorithm>
#include <config.h>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <sstream>

// Test this with:
//
// new win32-minimal --force --output c:\temp\NoMan --build-and-run
//

NewCommand::NewCommand() = default;

NewCommand::NewCommand(
    std::unique_ptr<AbstractCommand> nextCommand)
    : AbstractCommand(std::move(nextCommand))
{}

bool NewCommand::ParseCommandLine(
    const std::vector<std::string> &args)
{
    for (size_t i = 0; i < args.size(); i++)
    {
        const std::string &arg = args[i];

        if (arg == "new")
        {
            if (args.size() <= i + 1 || args[i + 1][0] == '-')
            {
                continue;
            }

            _templateName = args[i + 1];
        }
        else if (TestStringOptionInArgs(args, i, {"--name", "-n"}, _projectName))
        {
            // The name for the created output. If no name is specified, the name of the current directory is used.

            if (_projectName.empty())
            {
                return false;
            }

            continue;
        }
        else if (TestPathOptionInArgs(args, i, {"--output", "-o"}, _projectPath))
        {
            // Location to place the generated output. The default is the current directory.

            if (_projectPath.empty())
            {
                return false;
            }

            continue;
        }
        else if (arg == "--list" || arg == "-l")
        {
            // Lists templates containing the specified name. If no name is specified, lists all templates.

            _flagListTemplates = true;
        }
        else if (arg == "--force")
        {
            // Forces content to be generated even if it would change existing files. This is required when the template chosen would override existing files in the output directory.

            _flagForce = true;
        }
        else if (arg == "--build" || arg == "-b")
        {
            // Build the project after initializing

            _flagBuild = true;
        }
        else if (arg == "--build-and-run" || arg == "-r")
        {
            // Build and run the project after initializing

            _flagBuildAndRun = true;
        }
        else if (arg == "--stdout-as-json" || arg == "-j")
        {
            // Print the stdout as json object. Only works for --list.

            _flagOutputAsJson = true;
        }
        else if (TestHelpInArgs(args, i))
        {
            return false;
        }
    }

    if (!_flagListTemplates && _templateName.empty())
    {
        PrintCommandError();

        return false;
    }

    if (_projectPath.empty())
    {
        _projectPath = std::filesystem::current_path();
    }

    if (_flagBuild)
    {
        auto buildCommand = std::make_unique<BuildCommand>();
        std::vector<std::string> buildArgs{"build", "--project-path", _projectPath.string()};
        if (buildCommand->ParseCommandLine(buildArgs))
        {
            SetPostRunCommand(std::move(buildCommand));
        }
    }
    else if (_flagBuildAndRun)
    {
        auto runCommand = std::make_unique<RunCommand>();

        std::vector<std::string> runArgs{"run", "--project-path", _projectPath.string()};
        if (runCommand->ParseCommandLine(runArgs))
        {
            SetPostRunCommand(std::move(runCommand));
        }
    }

    return true;
}

int NewCommand::InternalRun()
{
    auto projectTemplates = ListTemplates();

    std::vector<unsigned char> zipFile = DownloadProjectTemplate(projectTemplates);

    // We only need to show the list of project templates
    if (_flagListTemplates)
    {
        if (_flagOutputAsJson)
        {
            std::cout << GetTemplates().dump(2) << std::endl;
        }
        else
        {
            std::cout << "The following project template are available:" << std::endl
                      << std::endl;

            for (auto &projectTemplate : projectTemplates)
            {
                std::cout << "* " << projectTemplate.first << std::endl;
            }

            std::cout << std::endl;
        }

        return 0;
    }

    if (!EnsureOutputDirectoryExists(_projectPath))
    {
        std::cerr << "Could not create the new output directory @ " << _projectPath << std::endl;

        return 0;
    }

    if (_projectName.empty())
    {
        _projectName = _projectPath.filename().string();
    }

    auto files = Utilities::ExtractZipFile(zipFile, _projectPath);

    if (!EnsureNoFilesAreOverwritten(files))
    {
        return 0;
    }

    Utilities::WriteAllFiles(files);

    std::cout << "The template '" << _projectName << "' was created successfully." << std::endl;

    PostInitUpdatesFiles();

    InitGit();

    return 0;
}

std::vector<unsigned char> NewCommand::DownloadProjectTemplate(
    std::map<std::string, ProjectTemplate> &projectTemplates)
{
    auto selectedTemplate = projectTemplates.find(_templateName);

    if (!_templateName.empty() && selectedTemplate == projectTemplates.end())
    {
        std::cerr << "Project template '" << _templateName << "' is not found in the list of available templates:" << std::endl
                  << std::endl;

        _flagListTemplates = true;
    }
    else if (selectedTemplate != projectTemplates.end())
    {
        std::stringstream ss;
        ss << "https://gitlab.com" << selectedTemplate->second.relative_path << "/-/archive/master/" << selectedTemplate->first << "-master.zip";
        auto zipFile = _httpClientGetTemplates.GetZip(ss.str());

        if (zipFile.empty())
        {
            std::cerr << "Failed to download project-template '" << _templateName << "'" << std::endl;
        }

        return zipFile;
    }

    return std::vector<unsigned char>();
}

bool NewCommand::EnsureNoFilesAreOverwritten(
    const std::map<std::filesystem::path, std::vector<unsigned char>> &files)
{
    if (files.empty())
    {
        std::cout << "Nothing to do..." << std::endl;

        return false;
    }

    if (_flagForce)
    {
        return true;
    }

    std::vector<std::string> overwriteFiles;

    for (auto &f : files)
    {
        if (std::filesystem::exists(f.first))
        {
            overwriteFiles.push_back(f.first.filename().string());
        }
    }

    if (!overwriteFiles.empty())
    {
        std::cerr << "Creating this template will make changes to existing files in " << _projectPath << std::endl;

        for (auto &f : overwriteFiles)
        {
            std::cerr << "  " << f << std::endl;
        }

        std::cerr << "Rerun the command and pass --force to accept and create." << std::endl;

        return false;
    }

    return true;
}

bool NewCommand::EnsureOutputDirectoryExists(
    std::filesystem::path &outputPath)
{
    if (outputPath.empty())
    {
        outputPath = std::filesystem::current_path();
    }

    if (!std::filesystem::exists(outputPath))
    {
        if (!std::filesystem::exists(outputPath.parent_path()))
        {
            return false;
        }

        std::filesystem::create_directory(outputPath);
    }

    return true;
}

void NewCommand::PostInitUpdatesFiles()
{
    auto postInitUpdateFilesFileName = (_projectPath / POST_INIT_UPDATE_FILES_FILENAME);

    if (std::filesystem::exists(postInitUpdateFilesFileName))
    {
        std::ifstream postInitUpdateFilesFileStream(postInitUpdateFilesFileName);
        std::vector<std::string> filesToUpdate;
        std::copy(std::istream_iterator<std::string>(postInitUpdateFilesFileStream),
                  std::istream_iterator<std::string>(),
                  std::back_inserter(filesToUpdate));
        postInitUpdateFilesFileStream.close();

        std::regex valueTemplateRegex(_templateName);
        for (auto &file : filesToUpdate)
        {
            auto fullFilePath = _projectPath / file;
            if (!std::filesystem::exists(fullFilePath))
            {
                std::cout << std::endl
                          << "Could not find " << fullFilePath << ", skipping update." << std::endl;
                continue;
            }

            std::ifstream fileStream(fullFilePath);
            std::string fileContent((std::istreambuf_iterator<char>(fileStream)),
                                    std::istreambuf_iterator<char>());
            fileStream.close();

            fileContent = std::regex_replace(fileContent, valueTemplateRegex, _projectName);

            std::ofstream fileOutputStream(fullFilePath);
            fileOutputStream << fileContent;
        }

        // Finally cleanup the autorun file
        std::filesystem::remove(postInitUpdateFilesFileName);
    }
}

void NewCommand::InitGit()
{
    Utilities::RunCommand("git init", _projectPath.string());

    Utilities::RunCommand("git add *", _projectPath.string());

    Utilities::RunCommand("git commit -m \"new project initiated with dotcpp\"", _projectPath.string());
}

void NewCommand::PrintCommandHelp()
{
    std::cout << "dotcpp version " << DOTCPP_VERSION << std::endl
              << "Usage: new <TEMPLATE> [options]" << std::endl
              << std::endl;

    std::cout << "<TEMPLATE>:" << std::endl
              << "  The template to instantiate." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -h, --help            Displays help for this command." << std::endl
              << "  -l, --list            Lists templates containing the specified template name. If no name is specified, lists all templates." << std::endl
              << "  -n, --name            The name for the output being created. If no name is specified, the name of the output directory is used." << std::endl
              << "  -o, --output          Location to place the generated output. The default is the current directory." << std::endl
              << "  --force               Forces content to be generated even if it would change existing files." << std::endl
              << "  -b, --build           Build the project after initializing." << std::endl
              << "  -r, --build-and-run   Build and run the project after initializing." << std::endl
              << "  -j, --stdout-as-json  Print the stdout as json object. Only works for --list." << std::endl;
}

void NewCommand::PrintCommandError()
{
    std::cerr << "Run dotcpp new --help for usage information." << std::endl
              << std::endl;

    PrintCommandHelp();
}
