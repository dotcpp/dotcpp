#include "buildcommand.h"

#include "../common/utils.h"
#include <config.h>
#include <fstream>
#include <iostream>
#include <sstream>

// Test this with:
//
// build NoMan --project-path c:\temp\NoMan --configuration Release
//

BuildCommand::BuildCommand() = default;

BuildCommand::BuildCommand(
    std::unique_ptr<AbstractCommand> nextCommand)
    : AbstractCommand(std::move(nextCommand))
{}

bool BuildCommand::ParseCommandLine(
    const std::vector<std::string> &args)
{
    for (size_t i = 0; i < args.size(); i++)
    {
        const std::string &arg = args[i];

        if (arg == "build")
        {
            if (args.size() <= i + 1 || args[i + 1][0] == '-')
            {
                continue;
            }
            else
            {
                _target = args[i + 1];
            }
        }
        else if (TestStringOptionInArgs(args, i, {"--configuration", "--config", "-c"}, _configuration))
        {
            if (_configuration.empty())
            {
                return false;
            }

            continue;
        }
        else if (TestPathOptionInArgs(args, i, {"--project-path", "-p"}, _projectPath))
        {
            if (_projectPath.empty())
            {
                return false;
            }

            continue;
        }
        else if (TestHelpInArgs(args, i))
        {
            return false;
        }
    }

    if (_projectPath.empty())
    {
        _projectPath = std::filesystem::current_path();
    }

    return true;
}

int BuildCommand::InternalRun()
{
    if (std::filesystem::exists(_projectPath / "CMakeLists.txt"))
    {
        return BuildCmakeProject();
    }

    return 0;
}

int BuildCommand::BuildCmakeProject()
{
    auto projectBuildPath = Utilities::GetBuildPathFromProjectPath(_projectPath);
    auto cmakeFileApiPath = projectBuildPath / ".cmake" / "api" / "v1" / "query";

    if (!std::filesystem::exists(cmakeFileApiPath))
    {
        std::filesystem::create_directories(cmakeFileApiPath);
        std::ofstream query(cmakeFileApiPath / "codemodel-v2");
    }

    std::stringstream ss;

    ss << "cmake " << std::filesystem::weakly_canonical(_projectPath).string() << " -B\"" << std::filesystem::weakly_canonical(projectBuildPath).string() << "\"";

    Utilities::RunCommand(ss.str(), std::filesystem::weakly_canonical(_projectPath).string());

    ss = std::stringstream();

    ss << "cmake --build \"" << std::filesystem::weakly_canonical(projectBuildPath).string() << "\"";

    if (!_target.empty())
    {
        ss << " --target " << _target;
    }

    if (!_configuration.empty())
    {
        ss << " --config " << _configuration;
    }

    Utilities::RunCommand(ss.str(), std::filesystem::weakly_canonical(_projectPath).string());

    return 0;
}

void BuildCommand::PrintCommandHelp()
{
    std::cout << "dotcpp version " << DOTCPP_VERSION << std::endl
              << "Usage: build <TARGET> [options]" << std::endl
              << std::endl;

    std::cout << "<TARGET>:" << std::endl
              << "  Name of the target to build." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -p, --project-path   Relative or absolute path to the root directory of the project." << std::endl
              << "  -c, --configuration  Name of the configuration to build." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -h, --help           Displays help for this command." << std::endl;
}

void BuildCommand::PrintCommandError()
{
    std::cerr << "Run dotcpp build --help for usage information." << std::endl
              << std::endl;

    PrintCommandHelp();
}
