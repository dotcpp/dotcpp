#include "cleancommand.h"

#include "../common/utils.h"
#include <config.h>
#include <iostream>
#include <sstream>

// Test this with:
//
// clean --project-path c:\temp\NoMan
//

CleanCommand::CleanCommand() = default;

CleanCommand::CleanCommand(
    std::unique_ptr<AbstractCommand> nextCommand)
    : AbstractCommand(std::move(nextCommand))
{}

bool CleanCommand::ParseCommandLine(
    const std::vector<std::string> &args)
{
    for (size_t i = 0; i < args.size(); i++)
    {
        const std::string &arg = args[i];

        if (arg == "clean")
        {
            if (args.size() <= i + 1 || args[i + 1][0] == '-')
            {
                continue;
            }
        }
        else if (TestPathOptionInArgs(args, i, {"--project-path", "-p"}, _projectPath))
        {
            continue;
        }
        else if (TestHelpInArgs(args, i))
        {
            return false;
        }
    }

    if (_projectPath.empty())
    {
        _projectPath = std::filesystem::current_path();
    }

    return true;
}

int CleanCommand::InternalRun()
{
    if (std::filesystem::exists(_projectPath / "CMakeLists.txt"))
    {
        return CleanCmakeProject();
    }

    return 0;
}

int CleanCommand::CleanCmakeProject()
{
    auto projectBuildPath = _projectPath.parent_path() / (_projectPath.filename().string() + "-build");
    std::stringstream ss;

    ss << "cmake --build \"" << std::filesystem::weakly_canonical(projectBuildPath).string() << "\" --target clean";

    Utilities::RunCommand(ss.str(), std::filesystem::weakly_canonical(_projectPath).string());

    return 0;
}

void CleanCommand::PrintCommandHelp()
{
    std::cout << "dotcpp version " << DOTCPP_VERSION << std::endl
              << "Usage: clean [options]" << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -p, --project-path   Relative or absolute path to the root directory of the project." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -h, --help           Displays help for this command." << std::endl;
}

void CleanCommand::PrintCommandError()
{
    std::cerr << "Run dotcpp build --help for usage information." << std::endl
              << std::endl;

    PrintCommandHelp();
}
