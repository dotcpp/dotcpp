#include "testcommand.h"

#include "../common/utils.h"
#include <config.h>
#include <iostream>

// Test this with:
//
// test --project-path c:\temp\NoMan
//

TestCommand::TestCommand()
{
}

TestCommand::TestCommand(
    std::unique_ptr<AbstractCommand> nextCommand)
    : AbstractCommand(std::move(nextCommand))
{}

bool TestCommand::ParseCommandLine(
    const std::vector<std::string> &args)
{
    for (size_t i = 0; i < args.size(); i++)
    {
        const std::string &arg = args[i];

        if (arg == "test")
        {
            if (args.size() <= i + 1 || args[i + 1][0] == '-')
            {
                continue;
            }
        }
        else if (TestPathOptionInArgs(args, i, {"--project-path", "-p"}, _projectPath))
        {
            if (_projectPath.empty())
            {
                return false;
            }

            continue;
        }
        else if (TestHelpInArgs(args, i))
        {
            return false;
        }
    }

    if (_projectPath.empty())
    {
        _projectPath = std::filesystem::current_path();
    }

    return true;
}

int TestCommand::InternalRun()
{
    auto projectBuildPath = Utilities::GetBuildPathFromProjectPath(_projectPath);

    Utilities::RunCommand("ctest", std::filesystem::weakly_canonical(projectBuildPath).string());

    return 0;
}

void TestCommand::PrintCommandHelp()
{
    std::cout << "dotcpp version " << DOTCPP_VERSION << std::endl
              << "Usage: test [options]" << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -p, --project-path   Relative or absolute path to the root directory of the project." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -h, --help           Displays help for this command." << std::endl;
}

void TestCommand::PrintCommandError()
{
    std::cerr << "Run dotcpp test --help for usage information." << std::endl
              << std::endl;

    PrintCommandHelp();
}
