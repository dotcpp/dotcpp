#ifndef NEWCOMMAND_H
#define NEWCOMMAND_H

#include "abstractcommand.h"

#include "../common/httpclient.h"
#include <filesystem>
#include <map>
#include <string>
#include <vector>

const char AUTORUN_CMAKE[] = "CMakeLists.txt";
const char POST_INIT_UPDATE_FILES_FILENAME[] = ".dotcpp-post-init-rename-files.txt";

class NewCommand : public AbstractCommand
{
public:
    NewCommand();

    NewCommand(
        std::unique_ptr<AbstractCommand> nextCommand);

    bool ParseCommandLine(
        const std::vector<std::string> &args);

protected:
    virtual int InternalRun();

    virtual void PrintCommandError();

    virtual void PrintCommandHelp();

private:
    HttpClient _httpClientGetTemplates;
    bool _flagListTemplates = false;
    bool _flagForce = false;
    bool _flagBuild = false;
    bool _flagBuildAndRun = false;
    bool _flagOutputAsJson = false;
    std::string _templateName;
    std::string _projectName;
    std::filesystem::path _projectPath;

    std::vector<unsigned char> DownloadProjectTemplate(
        std::map<std::string, ProjectTemplate> &projectTemplates);

    bool EnsureOutputDirectoryExists(
        std::filesystem::path &outputPath);

    bool EnsureNoFilesAreOverwritten(
        const std::map<std::filesystem::path, std::vector<unsigned char>> &files);

    void PostInitUpdatesFiles();

    void InitGit();
};

#endif // NEWCOMMAND_H
