#ifndef ABSTRACTCOMMAND_H
#define ABSTRACTCOMMAND_H

#include "../common/httpclient.h"
#include "../common/projecttemplate.h"
#include <filesystem>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

class AbstractCommand
{
public:
    AbstractCommand();

    AbstractCommand(
        std::unique_ptr<AbstractCommand> nextCommand);

    virtual ~AbstractCommand() = default;

    int Run();

    void SetPreRunCommand(
        std::unique_ptr<AbstractCommand> preRunCommand);

    void SetPostRunCommand(
        std::unique_ptr<AbstractCommand> postRunCommand);

protected:
    virtual int InternalRun() = 0;

    virtual void PrintCommandError() = 0;

    virtual void PrintCommandHelp() = 0;

    bool TestPathOptionInArgs(
        const std::vector<std::string> &args,
        size_t &i,
        const std::vector<std::string> &optionKeys,
        std::filesystem::path &path);

    bool TestStringOptionInArgs(
        const std::vector<std::string> &args,
        size_t &i,
        const std::vector<std::string> &optionKeys,
        std::string &value);

    bool TestIntOptionInArgs(
        const std::vector<std::string> &args,
        size_t &i,
        const std::vector<std::string> &optionKeys,
        int &value);

    bool TestHelpInArgs(
        const std::vector<std::string> &args,
        size_t i);

    HttpClient _httpClientGetTemplates;
    std::map<std::string, ProjectTemplate> ListTemplates();
    nlohmann::json GetTemplates();

private:
    int _localRunCount = 0;

    std::unique_ptr<AbstractCommand> _preRunCommand;
    std::unique_ptr<AbstractCommand> _postRunCommand;

    std::unique_ptr<AbstractCommand> &PreRunCommand();
    std::unique_ptr<AbstractCommand> &PostRunCommand();
};

class Commands
{
public:
    static std::unique_ptr<AbstractCommand> ParseCommandLine(
        const std::vector<std::string> &args);
};

#endif // ABSTRACTCOMMAND_H
