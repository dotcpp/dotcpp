#include "servecommand.h"

#include "../common/utils.h"
#include "buildcommand.h"
#include "cleancommand.h"
#include "newcommand.h"
#include "testcommand.h"
#include <config.h>
#include <cpp-httplib/httplib.h>
#include <htdocs.h>
#include <regex>
#include <sstream>

// Test this with:
//
// serve --project-path c:\temp\NoMan --port 79423
//

ServeCommand::ServeCommand() = default;

ServeCommand::ServeCommand(
    std::unique_ptr<AbstractCommand> nextCommand)
    : AbstractCommand(std::move(nextCommand))
{}

bool ServeCommand::ParseCommandLine(
    const std::vector<std::string> &args)
{
    for (size_t i = 0; i < args.size(); i++)
    {
        const std::string &arg = args[i];

        if (arg == "clean")
        {
            if (args.size() <= i + 1 || args[i + 1][0] == '-')
            {
                continue;
            }
        }
        else if (TestIntOptionInArgs(args, i, {"--port"}, _port))
        {
            continue;
        }
        else if (TestPathOptionInArgs(args, i, {"--project-path", "-p"}, _projectPath))
        {
            continue;
        }
        else if (TestHelpInArgs(args, i))
        {
            return false;
        }
    }

    if (_projectPath.empty())
    {
        _projectPath = std::filesystem::current_path();
    }

    return true;
}

int ServeCommand::InternalRun()
{
    if (std::filesystem::exists(_projectPath / "CMakeLists.txt"))
    {
        _state = States::Idle;
    }
    else
    {
        _state = States::EmptyFolder;
    }

    return ServeCmakeProject();
}

void AddSpa(
    httplib::Server &svr,
    const std::filesystem::path &projectPath)
{
    svr.Get("/", [&projectPath](const httplib::Request &, httplib::Response &res) {
        std::stringstream indexHtmlStream;
        indexHtmlStream << HTDOCS_INDEXHTML << HTDOCS_INDEXSECTION1HTML << HTDOCS_INDEXSECTION2HTML << HTDOCS_INDEXSECTION3HTML;
        std::string indexHtml = indexHtmlStream.str();

        Utilities::Replace(indexHtml, "[dotcpp-version]", DOTCPP_VERSION);
        Utilities::Replace(indexHtml, "[project-path]", projectPath.string());

        res.set_content(indexHtml, "text/html");
    });

    svr.Get("/help", [&projectPath](const httplib::Request &, httplib::Response &res) {
        std::string indexHtml = HTDOCS_HELPHTML;

        Utilities::Replace(indexHtml, "[dotcpp-version]", DOTCPP_VERSION);
        Utilities::Replace(indexHtml, "[project-path]", projectPath.string());

        res.set_content(indexHtml, "text/html");
    });

    svr.Get("/styles.css", [](const httplib::Request &, httplib::Response &res) {
        res.set_content(HTDOCS_STYLES, "text/css");
    });

    svr.Get("/scripts.js", [](const httplib::Request &, httplib::Response &res) {
        res.set_content(HTDOCS_SCRIPTS, "text/javascript");
    });
}

bool EnsureState(
    ServeCommand::States currentState,
    const std::vector<ServeCommand::States> &includedStates,
    httplib::Response &res)
{
    if (std::any_of(includedStates.begin(), includedStates.end(), [currentState](ServeCommand::States s) { return s == currentState; }))
    {
        return true;
    }

    res.status = 400;
    res.set_content("this request is not available in the current state", "text/plain");

    return false;
}

int ServeCommand::ServeCmakeProject()
{
    _codeModel.Load(_projectPath);

    httplib::Server svr;

    AddSpa(svr, _projectPath);

    svr.Get("/api/state$", [&](const httplib::Request &, httplib::Response &res) {
        res.set_content(GetState().dump(), "application/json");
    });

    svr.Get("/api/templates", [&](const httplib::Request &, httplib::Response &res) {
        if (!EnsureState(_state, {States::EmptyFolder}, res)) return;

        res.set_content(GetTemplates().dump(), "application/json");
    });

    svr.Post("/api/new-project$", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::EmptyFolder}, res)) return;

        nlohmann::json reqBody = nlohmann::json::parse(req.body);

        if (!reqBody.contains("template"))
        {
            res.status = 400;
            res.set_content("missing 'template' property", "text/plain");
            return;
        }

        res.status = 204;

        StartNewProjectCommand(reqBody["template"]);
    });

    svr.Get("/api/build$", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle}, res)) return;

        res.status = 204;

        std::string configuration;
        if (req.has_param("configuration"))
        {
            configuration = req.params.find("configuration")->second;
        }

        StartBuildCommand(configuration);
    });

    svr.Get("/api/clean$", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle}, res)) return;

        res.status = 204;

        std::string configuration;
        if (req.has_param("configuration"))
        {
            configuration = req.params.find("configuration")->second;
        }

        StartCleanCommand(configuration);
    });

    svr.Get("/api/test$", [&](const httplib::Request &, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle}, res)) return;

        res.status = 204;

        StartTestCommand();
    });

    svr.Get("/api/configurations$", [&](const httplib::Request &, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle, States::Building, States::Running, States::Testing}, res)) return;

        auto configurations = GetConfigurations();

        if (!configurations.empty())
        {
            res.set_content(configurations.dump(), "application/json");
        }
        else
        {
            res.set_content("[]", "application/json");
        }
    });

    svr.Get(R"(/api/configurations/([\w\-]+)/targets$)", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle, States::Building, States::Running, States::Testing}, res)) return;

        res.set_content(GetTargets(req.matches[1]).dump(), "application/json");
    });

    svr.Get(R"(/api/configurations/(\w+)/targets/([\w\-\_]+)/files$)", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle, States::Building, States::Running, States::Testing}, res)) return;

        res.set_content(GetTargetFiles(req.matches[1], req.matches[2]).dump(), "application/json");
    });

    svr.Post(R"(/api/configurations/(\w+)/targets/([\w\-\_]+)/files)", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle, States::Building, States::Running, States::Testing}, res)) return;

        auto jsonBody = nlohmann::json::parse(req.body);

        if (!jsonBody.contains("newfile") || jsonBody["newfile"] == "")
        {
            res.status = 400;

            res.set_content("missing mandatory parameter 'newfile'", "text/plain");

            return;
        }

        if (!jsonBody.contains("body"))
        {
            res.status = 400;

            res.set_content("missing mandatory parameter 'body'", "text/plain");

            return;
        }

        auto response = GetTargetFile(req.matches[1], req.matches[2], jsonBody["newfile"]);

        if (!response.empty())
        {
            res.status = 400;

            res.set_content("file already exists", "text/plain");

            return;
        }

        auto addFileResponse = AddFile(req.matches[1], req.matches[2], jsonBody["newfile"], jsonBody["body"]);
        if (addFileResponse.empty())
        {
            res.status = 200;
        }
        else
        {
            res.status = 500;

            res.set_content(addFileResponse.dump(), "application/json");
        }
    });

    svr.Get(R"(/api/configurations/(\w+)/targets/([\w\-\_]+)/files/(.+)$)", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle, States::Building, States::Running, States::Testing}, res)) return;

        auto response = GetTargetFile(req.matches[1], req.matches[2], req.matches[3]);

        if (response.empty())
        {
            res.status = 404;

            return;
        }

        res.set_content(response.dump(), "application/json");
    });

    svr.Put(R"(/api/configurations/(\w+)/targets/([\w\-\_]+)/files/(.+)$)", [&](const httplib::Request &req, httplib::Response &res) {
        if (!EnsureState(_state, {States::Idle, States::Building, States::Running, States::Testing}, res)) return;

        auto response = GetTargetFile(req.matches[1], req.matches[2], req.matches[3]);

        if (response.empty())
        {
            res.status = 404;

            return;
        }

        auto jsonBody = nlohmann::json::parse(req.body);

        if (!jsonBody.contains("body"))
        {
            res.status = 400;

            res.set_content("missing mandatory parameter 'body'", "text/plain");

            return;
        }

        auto updateFileResponse = UpdateFile(req.matches[1], req.matches[2], req.matches[3], jsonBody["body"]);
        if (updateFileResponse.empty())
        {
            res.status = 200;
        }
        else
        {
            res.status = 500;

            res.set_content(updateFileResponse.dump(), "application/json");
        }
    });

    std::cout << "serving project @ \"" << _projectPath.string() << "\" on port " << _port << std::endl
              << "Press ctrl+c to quit" << std::endl;

    svr.listen("0.0.0.0", _port);

    return 0;
}

void ServeCommand::StartBuildCommand(
    const std::string &configuration)
{
    if (_state.load() != States::Idle)
    {
        std::cout << "not in idle state" << std::endl;

        return;
    }

    std::vector<std::string> args = {
        "build",
        "--project-path",
        _projectPath.string(),
    };

    if (!configuration.empty())
    {
        args.push_back("--configuration");
        args.push_back(configuration);
    }

    StartCommand<BuildCommand>(States::Building, args);
}

void ServeCommand::StartCleanCommand(
    const std::string &configuration)
{
    if (_state.load() != States::Idle)
    {
        std::cout << "not in idle state" << std::endl;

        return;
    }

    std::vector<std::string> args = {
        "clean",
        "--project-path",
        _projectPath.string(),
    };

    if (!configuration.empty())
    {
        args.push_back("--configuration");
        args.push_back(configuration);
    }

    StartCommand<CleanCommand>(States::Cleaning, args);
}

void ServeCommand::StartTestCommand()
{
    if (_state.load() != States::Idle)
    {
        std::cout << "not in idle state" << std::endl;

        return;
    }

    std::vector<std::string> args = {
        "test",
        "--project-path",
        _projectPath.string(),
    };

    StartCommand<TestCommand>(States::Testing, args);
}

void ServeCommand::StartNewProjectCommand(
    const std::string &templateName)
{
    if (_state != States::EmptyFolder)
    {
        std::cout << "not an empty folder" << std::endl;

        return;
    }

    auto projectTemplates = ListTemplates();

    if (projectTemplates.find(templateName) == projectTemplates.end())
    {
        return;
    }

    std::vector<std::string> args = {
        "new",
        templateName,
        "--output",
        _projectPath.string(),
        "--build",
    };

    StartCommand<NewCommand>(States::Generating, args);
}

nlohmann::json ServeCommand::GetState()
{
    nlohmann::json r;

    States state = _state.load();

    if (state == States::EmptyFolder)
    {
        r["state"] = "EmptyFolder";
    }
    else if (state == States::Generating)
    {
        r["state"] = "GeneratingCmakeProject";
    }
    else if (state == States::Idle)
    {
        r["state"] = "IdleCmakeProject";
    }
    else if (state == States::Building)
    {
        r["state"] = "BuildingCmakeProject";
    }
    else if (state == States::Testing)
    {
        r["state"] = "TestingCmakeProject";
    }
    else if (state == States::Running)
    {
        r["state"] = "RunningCmakeProject";
    }

    return r;
}

nlohmann::json ServeCommand::GetConfigurations()
{
    auto configNames = _codeModel.GetConfigurationNames();

    nlohmann::json j(configNames);

    return j;
}

nlohmann::json ServeCommand::GetTargets(
    const std::string &configuration)
{
    auto targetNames = _codeModel.GetTargetNames(configuration);

    nlohmann::json j(targetNames);

    return j;
}

nlohmann::json ServeCommand::GetTargetFiles(
    const std::string &configuration,
    const std::string &targetName)
{
    auto targetFiles = _codeModel.GetTargetFiles(configuration, targetName);

    nlohmann::json j(targetFiles);

    return j;
}

nlohmann::json ServeCommand::GetTargetFile(
    const std::string &configuration,
    const std::string &targetName,
    const std::string &fileName)
{
    auto targetFiles = _codeModel.GetTargetFiles(configuration, targetName);

    auto found = std::find(
        targetFiles.begin(),
        targetFiles.end(), fileName);

    if (found == targetFiles.end())
    {
        return nlohmann::json();
    }

    auto fileContents = Utilities::LoadFile(_projectPath / fileName);

    nlohmann::json j = {
        {"fileName", fileName},
        {"fileContents", fileContents}};

    return j;
}

nlohmann::json ServeCommand::AddFile(
    const std::string &configuration,
    const std::string &targetName,
    const std::string &fileName,
    const std::string &body)
{
    return nlohmann::json({});
}

nlohmann::json ServeCommand::UpdateFile(
    const std::string &configuration,
    const std::string &targetName,
    const std::string &fileName,
    const std::string &body)
{
    return nlohmann::json({});
}

void ServeCommand::PrintCommandHelp()
{
    std::cout << "dotcpp version " << DOTCPP_VERSION << std::endl
              << "Usage: clean [options]" << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  --port               Http Port on which this project is served. Default is 8008" << std::endl
              << "  -p, --project-path   Relative or absolute path to the root directory of the project." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -h, --help           Displays help for this command." << std::endl;
}

void ServeCommand::PrintCommandError()
{
    std::cerr << "Run dotcpp build --help for usage information." << std::endl
              << std::endl;

    PrintCommandHelp();
}
