#include "runcommand.h"

#include "../common/codemodel.h"
#include "../common/utils.h"
#include "buildcommand.h"
#include <algorithm>
#include <config.h>
#include <fstream>
#include <iostream>
#include <regex>

// Test this with:
//
// run NoMan --project-path c:\temp\NoMan
//

RunCommand::RunCommand() = default;

RunCommand::RunCommand(
    std::unique_ptr<AbstractCommand> nextCommand)
    : AbstractCommand(std::move(nextCommand))
{}

bool RunCommand::ParseCommandLine(
    const std::vector<std::string> &args)
{
    for (size_t i = 0; i < args.size(); i++)
    {
        const std::string &arg = args[i];

        if (arg == "run")
        {
            if (args.size() <= i + 1 || args[i + 1][0] == '-')
            {
                continue;
            }
            else
            {
                _target = args[++i];
            }
        }
        else if (TestStringOptionInArgs(args, i, {"--configuration", "--config", "-c"}, _configuration))
        {
            if (_configuration.empty())
            {
                return false;
            }

            continue;
        }
        else if (TestPathOptionInArgs(args, i, {"--project-path", "-p"}, _projectPath))
        {
            if (_projectPath.empty())
            {
                return false;
            }

            continue;
        }
        else if (TestStringOptionInArgs(args, i, {"--target-to-run", "-t"}, _targetToRun))
        {
            if (_targetToRun.empty())
            {
                return false;
            }

            continue;
        }
        else if (TestHelpInArgs(args, i))
        {
            return false;
        }
    }

    if (_projectPath.empty())
    {
        _projectPath = std::filesystem::current_path();
    }

    auto buildCommand = std::make_unique<BuildCommand>();
    std::vector<std::string> buildArgs{"build", "--project-path", std::filesystem::weakly_canonical(_projectPath).string()};

    if (!_target.empty())
    {
        buildArgs.push_back("--target");
        buildArgs.push_back(_target);
    }

    if (!_configuration.empty())
    {
        buildArgs.push_back("--configuration");
        buildArgs.push_back(_configuration);
    }

    if (buildCommand->ParseCommandLine(buildArgs))
    {
        SetPreRunCommand(std::move(buildCommand));
    }

    return true;
}

std::vector<std::filesystem::path> RunCommand::FindRunnableArtifacts(
    const nlohmann::json &target,
    const nlohmann::json &configuration,
    const std::filesystem::path &cmakeFileApiReplyPath)
{
    std::vector<std::filesystem::path> result;

    auto targetReplyDocument = Utilities::LoadJsonFile(cmakeFileApiReplyPath / std::string(target["jsonFile"]));

    if (targetReplyDocument.is_object() && targetReplyDocument["artifacts"].is_array())
    {
        for (auto &artifact : targetReplyDocument["artifacts"])
        {
            if (artifact["path"].is_null()) continue;

            result.push_back(std::string(artifact["path"]));
        }
    }

    if (targetReplyDocument.is_object() && targetReplyDocument["dependencies"].is_array())
    {
        for (auto &dep : targetReplyDocument["dependencies"])
        {
            auto found = std::find_if(
                configuration["targets"].begin(),
                configuration["targets"].end(),
                [&](nlohmann::json const &v) {
                    return v["id"] == dep["id"];
                });

            if (found != configuration["targets"].end())
            {
                auto targetDocument = Utilities::LoadJsonFile(cmakeFileApiReplyPath / std::string((*found)["jsonFile"]));

                auto artifacts = FindRunnableArtifacts(*found, configuration, cmakeFileApiReplyPath);

                if (!artifacts.empty())
                {
                    for (auto artifact : artifacts)
                    {
                        if (!_targetToRun.empty() && artifact.string().find_first_of(_targetToRun) != std::string::npos)
                        {
                            result.push_back(artifact);
                        }

                        if (_targetToRun.empty() && artifact.extension() == ".exe")
                        {
                            result.push_back(artifact);
                        }
                    }
                }
            }
        }
    }

    return result;
}

int RunCommand::InternalRun()
{
    CodeModel codeModel;

    if (!codeModel.Load(_projectPath))
    {
        return 1;
    }

    auto selectedConfiguration = codeModel.GetConfiguration(_configuration);

    std::cout << "Selected configuration " << selectedConfiguration["name"] << std::endl;

    nlohmann::json selectedTarget = codeModel.GetTarget(_target, _configuration);

    std::cout << "Selected target " << selectedTarget["name"] << std::endl;

    auto cmakeFileApiReplyPath = codeModel.GetCmakeFileApiReplyPath();

    auto runnableArtifacts = FindRunnableArtifacts(selectedTarget, selectedConfiguration, cmakeFileApiReplyPath);

    if (runnableArtifacts.empty())
    {
        std::cerr << "No artifacts found in this target." << std::endl;

        return 1;
    }

    auto runnableFile = codeModel.GetBuildPath() / runnableArtifacts.front();

    if (!std::filesystem::exists(runnableFile))
    {
        std::cerr << "Found a artifact but it does not exist: " << std::filesystem::weakly_canonical(runnableFile).string() << std::endl;

        return 1;
    }

    Utilities::RunCommand(std::filesystem::weakly_canonical(runnableFile).string(), _projectPath.string());

    return 0;
}

void RunCommand::PrintCommandHelp()
{
    std::cout << "dotcpp version " << DOTCPP_VERSION << std::endl
              << "Usage: run <TARGET> [options]" << std::endl
              << std::endl;

    std::cout << "<TARGET>:" << std::endl
              << "  Name of the target to run." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -p, --project-path   Relative or absolute path to the root directory of the project." << std::endl
              << "  -c, --configuration  Name of the configuration to run." << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -h, --help           Displays help for this command." << std::endl;
}

void RunCommand::PrintCommandError()
{
    std::cerr << "Run dotcpp run --help for usage information." << std::endl
              << std::endl;

    PrintCommandHelp();
}
