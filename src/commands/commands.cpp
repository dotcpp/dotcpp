#include "abstractcommand.h"

#include "buildcommand.h"
#include "cleancommand.h"
#include "newcommand.h"
#include "runcommand.h"
#include "servecommand.h"
#include "testcommand.h"
#include <config.h>
#include <iostream>

void PrintError();

void PrintHelp();

AbstractCommand::AbstractCommand()
    : _postRunCommand(nullptr)
{}

AbstractCommand::AbstractCommand(
    std::unique_ptr<AbstractCommand> nextCommand)
    : _postRunCommand(std::move(nextCommand))
{}

std::map<std::string, ProjectTemplate> AbstractCommand::ListTemplates()
{
    std::map<std::string, ProjectTemplate> projectTemplates;

    nlohmann::json response = _httpClientGetTemplates.GetJson("https://gitlab.com/groups/dotcpp/project-templates/-/children.json");

    if (!response.is_array())
    {
        std::cerr << "Failed to retrieve the project template list" << std::endl;

        return std::map<std::string, ProjectTemplate>();
    }

    for (auto &item : response)
    {
        if (!item["name"].is_string() || !item["description"].is_string() || !item["relative_path"].is_string())
        {
            continue;
        }

        projectTemplates.insert(std::make_pair(item["name"], ProjectTemplate::FromJson(item)));
    }

    return projectTemplates;
}

nlohmann::json AbstractCommand::GetTemplates()
{
    auto projectTemplates = ListTemplates();

    nlohmann::json j;

    for (auto &projectTemplate : projectTemplates)
    {
        nlohmann::json pt = {
            {"name", projectTemplate.first},
            {"description", projectTemplate.second.description},
            {"relative-path", projectTemplate.second.relative_path},
        };

        j.push_back(pt);
    }

    return j;
}

bool AbstractCommand::TestPathOptionInArgs(
    const std::vector<std::string> &args,
    size_t &i,
    const std::vector<std::string> &optionKeys,
    std::filesystem::path &path)
{
    // shortcut to check if this is an option at all
    if (args[i][0] != '-')
    {
        return false;
    }

    bool argIsOption = std::any_of(
        optionKeys.begin(),
        optionKeys.end(),
        [&](const std::string &k) {
            return k == args[i];
        });

    if (!argIsOption)
    {
        return false;
    }

    if (args.size() <= i + 1 || args[i + 1][0] == '-')
    {
        std::cerr << args[i] << " needs a parameter" << std::endl
                  << std::endl;

        PrintCommandError();

        path.clear();

        return true;
    }

    path = args[++i];

    if (path.is_relative())
    {
        path = std::filesystem::current_path() / path;
    }

    return true;
}

bool AbstractCommand::TestStringOptionInArgs(
    const std::vector<std::string> &args,
    size_t &i,
    const std::vector<std::string> &optionKeys,
    std::string &value)
{
    // shortcut to check if this is an option at all
    if (args[i][0] != '-')
    {
        return false;
    }

    bool argIsOption = std::any_of(
        optionKeys.begin(),
        optionKeys.end(),
        [&](const std::string &k) {
            return k == args[i];
        });

    if (!argIsOption)
    {
        return false;
    }

    if (args.size() <= i + 1 || args[i + 1][0] == '-')
    {
        std::cerr << args[i] << " needs a parameter" << std::endl
                  << std::endl;

        PrintCommandError();

        value.clear();

        return true;
    }

    value = args[++i];

    return true;
}

bool AbstractCommand::TestIntOptionInArgs(
    const std::vector<std::string> &args,
    size_t &i,
    const std::vector<std::string> &optionKeys,
    int &value)
{
    // shortcut to check if this is an option at all
    if (args[i][0] != '-')
    {
        return false;
    }

    bool argIsOption = std::any_of(
        optionKeys.begin(),
        optionKeys.end(),
        [&](const std::string &k) {
            return k == args[i];
        });

    if (!argIsOption)
    {
        return false;
    }

    if (args.size() <= i + 1 || args[i + 1][0] == '-')
    {
        std::cerr << args[i] << " needs a parameter" << std::endl
                  << std::endl;

        PrintCommandError();

        return true;
    }

    value = atoi(args[++i].c_str());

    return true;
}

bool AbstractCommand::TestHelpInArgs(
    const std::vector<std::string> &args,
    size_t i)
{
    // shortcut to check if this is an option at all
    if (args[i][0] != '-')
    {
        return false;
    }

    if (args[i] != "--help" && args[i] != "-h")
    {
        return false;
    }

    // Prints out help for the command.

    PrintCommandHelp();

    return true;
}

int AbstractCommand::Run()
{
    if (_localRunCount != 0)
    {
        std::cerr << "A command should only run once!" << std::endl;

        return 666;
    }

    _localRunCount++;

    if (_preRunCommand != nullptr)
    {
        auto result = _preRunCommand->Run();
        if (result != 0)
        {
            return result;
        }
    }

    auto result = InternalRun();
    if (result != 0)
    {
        return result;
    }

    if (_postRunCommand != nullptr)
    {
        return _postRunCommand->Run();
    }

    return 0;
}

void AbstractCommand::SetPreRunCommand(
    std::unique_ptr<AbstractCommand> preRunCommand)
{
    _preRunCommand = std::move(preRunCommand);
}

void AbstractCommand::SetPostRunCommand(
    std::unique_ptr<AbstractCommand> postRunCommand)
{
    _postRunCommand = std::move(postRunCommand);
}

std::unique_ptr<AbstractCommand> &AbstractCommand::PreRunCommand()
{
    return _preRunCommand;
}

std::unique_ptr<AbstractCommand> &AbstractCommand::PostRunCommand()
{
    return _postRunCommand;
}

int AbstractCommand::InternalRun()
{
    return 0;
}

std::unique_ptr<AbstractCommand> Commands::ParseCommandLine(
    const std::vector<std::string> &args)
{
    if (args.empty())
    {
        PrintError();

        return nullptr;
    }

    if (args.front() == "new")
    {
        auto command = std::make_unique<NewCommand>();

        if (command->ParseCommandLine(args))
        {
            return command;
        }
    }
    else if (args.front() == "build")
    {
        auto command = std::make_unique<BuildCommand>();

        if (command->ParseCommandLine(args))
        {
            return command;
        }
    }
    else if (args.front() == "run")
    {
        auto command = std::make_unique<RunCommand>();

        if (command->ParseCommandLine(args))
        {
            return command;
        }
    }
    else if (args.front() == "test")
    {
        auto command = std::make_unique<TestCommand>();

        if (command->ParseCommandLine(args))
        {
            return command;
        }
    }
    else if (args.front() == "clean")
    {
        auto command = std::make_unique<CleanCommand>();

        if (command->ParseCommandLine(args))
        {
            return command;
        }
    }
    else if (args.front() == "serve")
    {
        auto command = std::make_unique<ServeCommand>();

        if (command->ParseCommandLine(args))
        {
            return command;
        }
    }
    else if (args.front() == "--help" || args.front() == "-h")
    {
        PrintHelp();
    }
    else
    {
        PrintError();
    }

    return nullptr;
}

void PrintError()
{
    std::cerr << "Run dotcpp --help for usage information." << std::endl
              << std::endl;

    PrintHelp();
}

void PrintHelp()
{
    std::cout << "dotcpp version " << DOTCPP_VERSION << std::endl
              << "Usage: <COMMAND> [options]" << std::endl
              << std::endl;

    std::cout << "<COMMAND>:" << std::endl
              << "  The following commands are supported." << std::endl
              << "  * new" << std::endl
              << "  * build" << std::endl
              << "  * run" << std::endl
              << "  * test " << std::endl
              << "  * clean " << std::endl
              << std::endl;

    std::cout << "Options:" << std::endl
              << "  -h, --help          Displays help." << std::endl
              << std::endl;
}
