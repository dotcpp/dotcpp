#include "commands/abstractcommand.h"

int main(
    int argc,
    char *argv[])
{
    const std::vector<std::string> args(argv + 1, argv + argc);

    auto command = Commands::ParseCommandLine(args);

    if (command == nullptr)
    {
        return 0;
    }

    return command->Run();
}
