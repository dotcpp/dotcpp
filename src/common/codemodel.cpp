#include "codemodel.h"

#include "utils.h"
#include <iostream>

CodeModel::CodeModel()
{}

bool CodeModel::Load(
    const std::filesystem::path &projectPath)
{
    _projectPath = projectPath;

    std::filesystem::path path_found;

    auto cmakeFileApiReplyPath = GetCmakeFileApiReplyPath();

    if (!Utilities::FindFileByRegex(cmakeFileApiReplyPath, std::regex("codemodel-v2-[0-9a-z]*.json"), path_found))
    {
        std::cerr << "Could not find codemodel file." << std::endl;

        return false;
    }

    _codeModel = Utilities::LoadJsonFile(path_found);

    if (!_codeModel.is_object())
    {
        std::cerr << "Failed to load codemodel." << std::endl;

        return false;
    }

    return true;
}

std::filesystem::path CodeModel::GetBuildPath() const
{
    return _projectPath.parent_path() / (_projectPath.filename().string() + "-build");
}

std::filesystem::path CodeModel::GetCmakeFileApiReplyPath() const
{
    return GetBuildPath() / ".cmake" / "api" / "v1" / "reply";
}

std::vector<std::string> CodeModel::GetConfigurationNames() const
{
    std::vector<std::string> result;

    if (_codeModel.empty())
    {
        return result;
    }

    std::transform(
        _codeModel["configurations"].begin(),
        _codeModel["configurations"].end(),
        std::back_inserter(result),
        [](nlohmann::json j) -> std::string {
            return j["name"].get<std::string>();
        });

    return result;
}

std::vector<std::string> CodeModel::GetTargetNames(
    const std::string &configuration) const
{
    std::vector<std::string> result;

    auto conf = GetConfiguration(configuration);

    std::transform(
        conf["targets"].begin(),
        conf["targets"].end(),
        std::back_inserter(result),
        [](nlohmann::json j) -> std::string {
            return j["name"].get<std::string>();
        });

    return result;
}

nlohmann::json CodeModel::GetConfiguration(
    const std::string &configuration) const
{
    if (!configuration.empty())
    {
        auto found = std::find_if(
            _codeModel["configurations"].begin(),
            _codeModel["configurations"].end(),
            [&](nlohmann::json const &v) {
                return v["name"] == configuration;
            });

        if (found != _codeModel["configurations"].end())
        {
            return *found;
        }
    }

    return _codeModel["configurations"].front();
}

nlohmann::json CodeModel::GetTarget(
    const std::string &configuration,
    const std::string &target) const
{
    auto selectedConfiguration = GetConfiguration(configuration);

    if (!target.empty())
    {
        auto found = std::find_if(
            selectedConfiguration["targets"].begin(),
            selectedConfiguration["targets"].end(),
            [&](nlohmann::json const &v) {
                return v["name"] == target;
            });

        if (found != selectedConfiguration["targets"].end())
        {
            return *found;
        }
    }

    return selectedConfiguration["targets"].front();
}

std::vector<std::string> CodeModel::GetTargetFiles(
    const std::string &configuration,
    const std::string &target) const
{
    auto targetNode = GetTarget(configuration, target);

    auto targetModelFile = GetCmakeFileApiReplyPath() / targetNode["jsonFile"].get<std::string>();

    auto targetModel = Utilities::LoadJsonFile(targetModelFile);

    std::vector<std::string> result;
    std::transform(
        targetModel["sources"].begin(),
        targetModel["sources"].end(),
        std::back_inserter(result),
        [](nlohmann::json j) -> std::string {
            return j["path"].get<std::string>();
        });

    return result;
}
