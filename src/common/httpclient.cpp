#include "httpclient.h"

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <wininet.h>

#include <iostream>
#include <sstream>

HttpClient::HttpClient()
    : hInternet(InternetOpenA("wininet-test", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0))
{}

HttpClient::~HttpClient()
{
    InternetCloseHandle(hInternet);
}

HINTERNET OpenUrl(
    HINTERNET appHandle,
    const std::string &from);

std::vector<unsigned char> HttpClient::GetZip(
    const std::string &from)
{
    HINTERNET hUrl = OpenUrl(
        hInternet,
        from);

    if (hUrl == NULL)
    {
        return std::vector<unsigned char>();
    }

    std::vector<unsigned char> buffer;

    while (1)
    {
        DWORD br = 0;
        unsigned char buf[1024 * 64];
        if (!InternetReadFile(hUrl, buf, sizeof(buf), &br))
        {
            std::cerr << "InternetReadFile failed. err=" << (int)GetLastError() << std::endl;
            break;
        }
        else if (br == 0)
        {
            break; /* done! */
        }
        else
        {
            buffer.insert(buffer.end(), buf, buf + br);
        }
    }

    return buffer;
}

nlohmann::json HttpClient::GetJson(
    const std::string &from)
{
    HINTERNET hUrl = OpenUrl(
        hInternet,
        from);

    if (hUrl == NULL)
    {
        return nlohmann::json();
    }

    std::stringstream ss;

    while (1)
    {
        DWORD br = 0;
        BYTE buf[1024 * 64];
        if (!InternetReadFile(hUrl, buf, sizeof(buf), &br))
        {
            std::cerr << "InternetReadFile failed. err=" << (int)GetLastError() << std::endl;
            break;
        }
        else if (br == 0)
        {
            break; /* done! */
        }
        else
        {
            ss.write((char *)buf, br);
        }
    }

    InternetCloseHandle(hUrl);

    if (ss.str().empty())
    {
        return nlohmann::json();
    }

    return nlohmann::json::parse(ss.str());
}

HINTERNET OpenUrl(
    HINTERNET appHandle,
    const std::string &from)
{
    DWORD httpcode = 0;
    DWORD dwordlen = sizeof(DWORD);
    DWORD zero = 0;
    HINTERNET hUrl = InternetOpenUrlA(
        appHandle,
        from.c_str(),
        NULL,
        0,
        INTERNET_FLAG_HYPERLINK |
            INTERNET_FLAG_IGNORE_REDIRECT_TO_HTTP |
            INTERNET_FLAG_IGNORE_REDIRECT_TO_HTTPS |
            INTERNET_FLAG_NO_CACHE_WRITE |
            INTERNET_FLAG_NO_COOKIES |
            INTERNET_FLAG_NO_UI |
            INTERNET_FLAG_RESYNCHRONIZE |
            INTERNET_FLAG_RELOAD |
            INTERNET_FLAG_SECURE,
        0);

    if (!hUrl)
    {
        std::cerr << "InternetOpenUrl failed. err=" << (int)GetLastError() << std::endl;
    }
    else if (!HttpQueryInfo(hUrl, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &httpcode, &dwordlen, &zero))
    {
        std::cerr << "HttpQueryInfo failed. err=" << (int)GetLastError() << std::endl;
    }
    else if (httpcode != 200)
    {
        std::cerr << "HTTP request failed with response code " << (int)httpcode << std::endl;
    }
    else
    {
        return hUrl;
    }

    InternetCloseHandle(hUrl);

    return NULL;
}
