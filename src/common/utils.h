#ifndef UTILS_H
#define UTILS_H

#include "../common/httpclient.h"
#include <filesystem>
#include <map>
#include <nlohmann/json.hpp>
#include <regex>
#include <vector>

class Utilities
{
public:
    static std::map<std::filesystem::path, std::vector<unsigned char>> ExtractZipFile(
        const std::vector<unsigned char> &zipFile,
        const std::filesystem::path &destination);

    static void WriteAllFiles(
        const std::map<std::filesystem::path, std::vector<unsigned char>> &files);

    static void RunCommand(
        const std::string &command,
        const std::string &workingDirectory);

    static std::string Utilities::LoadFile(
        std::filesystem::path const &path);

    static nlohmann::json LoadJsonFile(
        std::filesystem::path const &path);

    static bool FindFileByRegex(
        const std::filesystem::path &dir_path,
        const std::regex &regex,
        std::filesystem::path &path_found);

    static bool FindFileByFileName(
        const std::filesystem::path &dir_path,
        const std::string &filename,
        std::filesystem::path &path_found);

    static std::filesystem::path GetBuildPathFromProjectPath(
        const std::filesystem::path &projectPath);

    static bool Replace(
        std::string& str,
        const std::string& from,
        const std::string& to);
};

#endif // UTILS_H
