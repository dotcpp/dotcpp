#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <nlohmann/json.hpp>
#include <vector>

class HttpClient
{
    void* hInternet = nullptr;

public:
    HttpClient();

    ~HttpClient();

    std::vector<unsigned char> GetZip(
        const std::string &from);

    nlohmann::json GetJson(
        const std::string &from);
};

#endif // HTTPCLIENT_H
