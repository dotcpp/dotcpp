#include "utils.h"

#include <fstream>
#include <iostream>
#include <miniz/miniz.h>

std::map<std::filesystem::path, std::vector<unsigned char>> Utilities::ExtractZipFile(
    const std::vector<unsigned char> &zipFile,
    const std::filesystem::path &destination)
{
    std::map<std::filesystem::path, std::vector<unsigned char>> result;

    if (!std::filesystem::exists(destination))
    {
        std::cerr << "Target location " << destination << " does not exist." << std::endl;

        return result;
    }

    mz_zip_archive zip_archive;

    memset(&zip_archive, 0, sizeof(zip_archive));
    auto status = mz_zip_reader_init_mem(&zip_archive, zipFile.data(), zipFile.size(), 0);
    if (!status)
    {
        std::cerr << "Failed to open zip file" << std::endl;

        return result;
    }

    std::string rootDirectory;

    if (mz_zip_reader_is_file_a_directory(&zip_archive, 0))
    {
        mz_zip_archive_file_stat root_dir_stat;
        if (!mz_zip_reader_file_stat(&zip_archive, 0, &root_dir_stat))
        {
            printf("mz_zip_reader_file_stat() failed for root directory!\n");

            mz_zip_reader_end(&zip_archive);

            return result;
        }

        rootDirectory = root_dir_stat.m_filename;
    }

    // Get and print information about each file in the archive.
    for (unsigned int i = 1; i < mz_zip_reader_get_num_files(&zip_archive); i++)
    {
        mz_zip_archive_file_stat file_stat;
        if (!mz_zip_reader_file_stat(&zip_archive, i, &file_stat))
        {
            std::cerr << "failed to read entry '" << i << "' from zip file" << std::endl;

            break;
        }

        auto newpath = destination / std::filesystem::path(file_stat.m_filename + rootDirectory.size());

        if (mz_zip_reader_is_file_a_directory(&zip_archive, i))
        {
            if (!std::filesystem::exists(newpath))
            {
                std::filesystem::create_directory(newpath);
            }

            continue;
        }

        size_t uncomp_size = file_stat.m_uncomp_size;

        void *p = mz_zip_reader_extract_file_to_heap(&zip_archive, file_stat.m_filename, &uncomp_size, 0);

        if (p == nullptr)
        {
            std::cerr << "failed to read '" << file_stat.m_filename << "' from zip file" << std::endl;

            continue;
        }

        std::vector<unsigned char> localfile;

        localfile.insert(localfile.end(), (unsigned char *)p, ((unsigned char *)p) + uncomp_size);

        result.insert(std::make_pair(newpath, localfile));

        mz_free(p);
    }

    mz_zip_reader_end(&zip_archive);

    return result;
}

void Utilities::WriteAllFiles(
    const std::map<std::filesystem::path, std::vector<unsigned char>> &files)
{
    for (auto &f : files)
    {
        std::ofstream outfile(f.first, std::ofstream::binary);

        if (!outfile.is_open())
        {
            std::cerr << "Failed to open " << f.first << " for writing" << std::endl;
        }
        else
        {
            outfile.write((char *)f.second.data(), f.second.size());
        }
    }
}

nlohmann::json Utilities::LoadJsonFile(
    std::filesystem::path const &path)
{
    auto doc = nlohmann::json::parse(LoadFile(path));

    if (doc.is_null())
    {
        std::cerr << "Could not open json file." << std::endl;

        return nlohmann::json(nullptr);
    }

    return doc;
}

std::string Utilities::LoadFile(
    std::filesystem::path const &path)
{
    std::ifstream fileStream(path);

    std::string content((std::istreambuf_iterator<char>(fileStream)),
                        std::istreambuf_iterator<char>());

    return content;
}

bool Utilities::FindFileByRegex(
    const std::filesystem::path &dir_path, // in this directory,
    const std::regex &regex,               // search for this name,
    std::filesystem::path &path_found)     // placing path here if found
{
    if (!exists(dir_path))
    {
        return false;
    }

    std::filesystem::directory_iterator end_itr; // default construction yields past-the-end
    for (std::filesystem::directory_iterator itr(dir_path); itr != end_itr; ++itr)
    {
        if (is_directory(itr->status()))
        {
            if (FindFileByRegex(itr->path(), regex, path_found)) return true;
        }
        else if (std::regex_match(itr->path().filename().string(), regex))
        {
            path_found = itr->path();
            return true;
        }
    }
    return false;
}

bool Utilities::FindFileByFileName(
    const std::filesystem::path &dir_path, // in this directory,
    const std::string &filename,           // search for this name,
    std::filesystem::path &path_found)     // placing path here if found
{
    if (!exists(dir_path))
    {
        return false;
    }

    std::filesystem::directory_iterator end_itr; // default construction yields past-the-end
    for (std::filesystem::directory_iterator itr(dir_path); itr != end_itr; ++itr)
    {
        if (is_directory(itr->status()))
        {
            if (FindFileByFileName(itr->path(), filename, path_found)) return true;
        }
        else if (itr->path().filename() == filename)
        {
            path_found = itr->path();
            return true;
        }
    }
    return false;
}

// From https://stackoverflow.com/a/3418285
bool Utilities::Replace(
    std::string& str,
    const std::string& from,
    const std::string& to)
{
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
    {
        return false;
    }

    str.replace(start_pos, from.length(), to);

    return true;
}

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

void Utilities::RunCommand(
    const std::string &command,
    const std::string &workingDirectory)
{
    std::cout << "+-----------------------------------------------" << std::endl
              << "| Running" << std::endl
              << "|   " << command << std::endl
              << "| From" << std::endl
              << "|   " << workingDirectory << std::endl
              << "+-----------------------------------------------" << std::endl;

    // additional information
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    // set the size of the structures
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    std::string localCommand = command;

    // start the program up
    auto result = CreateProcess(
        NULL,                                     // the path
        const_cast<char *>(localCommand.c_str()), // Command line
        NULL,                                     // Process handle not inheritable
        NULL,                                     // Thread handle not inheritable
        FALSE,                                    // Set handle inheritance to FALSE
        0,                                        // No creation flags
        NULL,                                     // Use parent's environment block
        workingDirectory.c_str(),                 // Use given starting directory
        &si,                                      // Pointer to STARTUPINFO structure
        &pi                                       // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
    );

    if (result == FALSE)
    {
        std::cerr << "error " << GetLastError() << " after running " << command << std::endl
                  << std::endl;

        return;
    }

    WaitForSingleObject(pi.hProcess, INFINITE);

    // Close process and thread handles.
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    std::cout << std::endl;
}

std::filesystem::path Utilities::GetBuildPathFromProjectPath(
    const std::filesystem::path &projectPath)
{
    auto pp = projectPath.string();

    while ((pp.back() == '/') || (pp.back() == '\\'))
        pp.erase(pp.size() - 1);

    return std::filesystem::path(pp + "-build");
}
