#ifndef PROJECTTEMPLATE_H
#define PROJECTTEMPLATE_H

#include <nlohmann/json.hpp>
#include <string>

class ProjectTemplate
{
public:
    ProjectTemplate();

    std::string name;

    std::string description;

    std::string relative_path;

    static ProjectTemplate FromJson(
        nlohmann::json &item);
};

#endif // PROJECTTEMPLATE_H
