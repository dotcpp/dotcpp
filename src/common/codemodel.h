#ifndef CODEMODEL_H
#define CODEMODEL_H

#include <filesystem>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

class CodeModel
{
public:
    CodeModel();

    bool Load(
        const std::filesystem::path &projectPath);

    std::filesystem::path GetBuildPath() const;

    std::filesystem::path GetCmakeFileApiReplyPath() const;

    std::vector<std::string> GetConfigurationNames() const;

    std::vector<std::string> GetTargetNames(
        const std::string &configuration = "") const;

    nlohmann::json GetConfiguration(
        const std::string &configuration) const;

    nlohmann::json GetTarget(
        const std::string &configuration,
        const std::string &target) const;

    std::vector<std::string> GetTargetFiles(
        const std::string &configuration,
        const std::string &target) const;

private:
    std::filesystem::path _projectPath;
    nlohmann::json _codeModel;
};

#endif // CODEMODEL_H
