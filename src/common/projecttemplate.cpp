#include "projecttemplate.h"

ProjectTemplate::ProjectTemplate()
{
}

ProjectTemplate ProjectTemplate::FromJson(
    nlohmann::json &item)
{
    if (!item["name"].is_string() || !item["description"].is_string() || !item["relative_path"].is_string())
    {
        ProjectTemplate();
    }

    ProjectTemplate pt;

    pt.name = item["name"];
    pt.description = item["description"];
    pt.relative_path = item["relative_path"];

    return pt;
}
