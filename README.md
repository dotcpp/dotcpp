# dotcpp

A bootstrap tool for c++/cmake projects.

With dotcpp you can quickly create a c++ project from one of the templates in the [project-templates](https://gitlab.com/dotcpp/project-templates) group of this project. It contains templates for win32, OpenGL and GLFW. Some make use of CPM for package management. All of them use cmake for build management. To check out all the available projects, try ``dotcpp new --list`` in your windows cmd. Make sure ``dotcpp.exe`` is available in the your PATH.

```
c:\temp>dotcpp new --list
The following project template are available:

* glfw-imgui-minimal
* glfw-minimal
* sdl2-cube
* sdl2-entt-bullet
* sdl2-entt-bullet-imgui
* sdl2-minimal
* win32-gl-minimal
* win32-gl-triangle
* win32-imgui-minimal
* win32-minimal
```

When you know what template to use for your new c++ project, try ``dotcpp new win32-gl-triangle -o test-triangle`` in the windows cmd:

```
C:\temp>dotcpp new win32-gl-triangle -o test-triangle
The template 'test-triangle' was created successfully.

Auto running powershell.exe "C:\temp\test-triangle\.auto-run-post-init.ps1 test-triangle"...
Done auto run post init.
```

This will use the ``win32-gl-triangle`` template for the new project. The project is create in the ``test-triangle`` folder relative to the current directory, in this case ``C:\temp\test-triangle\``. You can now open your c++ project with the IDE of your choice.

## Help

Dotcpp has the following general help page:

```
c:\temp>dotcpp --help
dotcpp version 1.1.0
Usage: <COMMAND> [options]

<COMMAND>:
  The following commands are supported.
  * new
  * build
  * run
  * test
  * clean

Options:
  -h, --help          Displays help.
```

Help page for the ``new`` command:

```
c:\temp>dotcpp new --help
dotcpp version 1.1.0
Usage: new <TEMPLATE> [options]

<TEMPLATE>:
  The template to instantiate.

Options:
  -h, --help           Displays help for this command.
  -l, --list           Lists templates containing the specified template name. If no name is specified, lists
                       all templates.
  -n, --name           The name for the output being created. If no name is specified, the name of the output
                       directory is used.
  -o, --output         Location to place the generated output. The default is the current directory.
  --force              Forces content to be generated even if it would change existing files.
  -b, --build          Build the project after initializing.
  -r, --build-and-run  Build and run the project after initializing.
```

Help page for the ``build`` command:

```
c:\temp>dotcpp build --help
dotcpp version 1.1.0
Usage: build <TARGET> [options]

<TARGET>:
  Name of the target to build.

Options:
  -p, --project-path   Relative or absolute path to the root directory of the project.
  -c, --configuration  Name of the configuration to build.

Options:
  -h, --help           Displays help for this command.
```

Help page for the ``run`` command:

```
c:\temp>dotcpp run --help
dotcpp version 1.1.0
Usage: run <TARGET> [options]

<TARGET>:
  Name of the target to run.

Options:
  -p, --project-path   Relative or absolute path to the root directory of the project.
  -c, --configuration  Name of the configuration to run.

Options:
  -h, --help           Displays help for this command.
```

Help page for the ``test`` command:

```
c:\temp>dotcpp run --help
dotcpp version 1.1.0
Usage: run <TARGET> [options]

<TARGET>:
  Name of the target to run.

Options:
  -p, --project-path   Relative or absolute path to the root directory of the project.
  -c, --configuration  Name of the configuration to run.

Options:
  -h, --help           Displays help for this command.
```

Help page for the ``clean`` command:

```
c:\temp>dotcpp clean --help
dotcpp version 1.1.0
Usage: clean [options]

Options:
  -p, --project-path   Relative or absolute path to the root directory of the project.

Options:
  -h, --help           Displays help for this command.
```
