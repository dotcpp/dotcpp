#include "acutest.h"

#include <common/utils.h>
#include <filesystem>

void test_GetBuildPathFromProjectPath1(void)
{
    std::filesystem::path projectDir("c:\\temp\\project-dir\\");

    auto buildDir = Utilities::GetBuildPathFromProjectPath(projectDir);

    TEST_CHECK(buildDir.string() == "c:\\temp\\project-dir-build");
}

void test_GetBuildPathFromProjectPath2(void)
{
    std::filesystem::path projectDir("c:\\temp\\project-dir\\this-is-a-file.txt");

    auto buildDir = Utilities::GetBuildPathFromProjectPath(projectDir);

    TEST_CHECK(buildDir.string() == "c:\\temp\\project-dir-build");
}

TEST_LIST = {
    { "GetBuildPathFromProjectPath should work with path seperators at the end", test_GetBuildPathFromProjectPath1 },
    { "GetBuildPathFromProjectPath should work on path with filename", test_GetBuildPathFromProjectPath2 },
   { NULL, NULL }     /* zeroed record marking the end of the list */
};
